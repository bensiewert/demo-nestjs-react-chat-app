# Demo Nest.js + React Chat App

# getting started

1. setup mailtrap.io and enter credentials in env file
2. ```git clone git@gitlab.com:bensiewert/demo-nestjs-react-chat-app.git```
3. ```cd chat-app && npm i```
4. ```docker-compose up --build```
5. ```cd ..```
6. ```cd chat-app-ui && npm i```
7. ``` npm run dev```
8. go to [localhost:3000](http://localhost:3000)
9. register account
10. verify email on mailtrap
11. log in

# Techonologies used
1. Nest.js
2. React.js
3. Docker & Docker Compose
4. Postgres
5. Prometheus
6. Grafana

# Todo 
- [ ] Setup ci/cd pipelines for all.
- [ ] Persist chat users and messages.
- [ ] Invites/friends.
- [ ] Chat groups.
- [ ] Move socket.io server to its own service.
- [ ] Setup automated websocket tests.
- [ ] Finish prometheus alert manager, logs and grafana metrics. Add visibility into websockets.
- [ ] Setup production Docker files.
- [ ] Setup task queue and move emails and other background tasks to it.
- [ ] Finish load tests for both REST API and websocket server.
- [ ] Deploy to Kuberenetes cluster.
- [ ] Refactor react app with new tests a for better arch there.


![demo chat app](/docs/images/screenshot.PNG)