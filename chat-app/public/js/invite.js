console.log('invite!');

async function savePassword() {
  console.log('save password');
  const password = document.getElementById('password').value;
  const confirmPassword = document.getElementById('confirmPassword').value;
  const params = new URLSearchParams(window.location.search);
  const token = params.get('token');
  console.log('password: ', confirmPassword, token);
  try {
    const response = await fetch({
      method: 'POST',
      url: 'http://localhost:3000/auth/invite-complete',
      body: { password: password, confirmPassword: confirmPassword },
    });
    console.log('response: ', response);
  } catch (error) {
    console.log('error: ', error);
  }
}
