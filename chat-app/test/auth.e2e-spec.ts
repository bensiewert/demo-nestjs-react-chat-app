import { Test, TestingModule } from '@nestjs/testing';
import { INestApplication } from '@nestjs/common';
import * as request from 'supertest';
import { AppModule } from './../src/app.module';

describe('AuthController (e2e)', () => {
  let app: INestApplication;
  // let authService = {  }

  beforeAll(async () => {
    const moduleFixture: TestingModule = await Test.createTestingModule({
      imports: [AppModule],
    }).compile();

    app = moduleFixture.createNestApplication();
    await app.init();
  });

  it('it should register a user', () => {
    return request(app.getHttpServer())
      .post('/auth/register')
      .send({
        email: 'testz@test.com',
        firstName: 'test',
        lastName: 'test',
        password: 'password',
      })
      .expect(201)
      .expect('Registration successful. Please confirm your email.');
  });
});
