from __future__ import absolute_import
from __future__ import unicode_literals
from __future__ import print_function

import json
import uuid
import time
import gevent
import six
from websocket import create_connection
from locust import HttpUser, TaskSet, FastHttpUser, task, between



# class SomeHttpMethods(FastHttpUser):
#     wait_time = between(1, 5)

#     @task
#     def hello_world(self):
#         self.client.get("/")

#     # @task(3)
#     # def view_items(self):
#     #     for item_id in range(10):
#     #         self.client.get(f"/item?id={item_id}", name="/item")
#     #         time.sleep(1)

#     # def on_start(self):
#     #     self.client.post("/login", json={"username":"foo", "password":"bar"})
    

class EchoLocust(HttpUser):
    def on_start(self):
        self.user_id = six.text_type(uuid.uuid4())
        ws = create_connection('ws://localhost:8080')
        self.ws = ws

        def _receive():
            while True:
                res = ws.recv()
                print('res: ', res)
                data = json.loads(res)
                end_at = time.time()
                response_time = int((end_at - data['start_at']) * 1000000)

        gevent.spawn(_receive)

    def on_quit(self):
        self.ws.close()

    @task
    def sent(self):
        start_at = time.time()
        body = json.dumps({'message': 'hello, world', 'user_id': self.user_id, 'start_at': start_at})
        self.ws.send(body)
