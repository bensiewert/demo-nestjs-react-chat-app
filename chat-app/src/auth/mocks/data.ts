import { JwtModuleOptions } from '@nestjs/jwt';
import { CreateUserDto } from 'src/users/dto/create-user.dto';
import { ExistingUserDto } from 'src/users/dto/existing-user.dto';

export const MOCK_USER = {
  id: 'user-id',
  username: 'usernamez',
  email: 'test@test.com',
};

export const MOCK_USER_FOR_CREATE: CreateUserDto = {
  username: 'usernamez',
  email: 'test@test.com',
  password: 'password',
  confirmPassword: 'password',
};

export const MOCK_EXISTING_USER: ExistingUserDto = {
  id: 'user-id',
  username: 'usernamez',
  email: 'existing@test.com',
  isActive: true,
  confirmEmailToken: null,
  password: null,
};

export const MOCK_JWT_REGISTER: JwtModuleOptions = {
  secret: 'asdf',
  secretOrPrivateKey: 'asdf',
  signOptions: {
    expiresIn: '60s',
  },
};

export const MOCK_LOGIN_RESPONSE = {
  access_token: 'jwt',
};
