import { MOCK_EXISTING_USER, MOCK_USER } from './data';

export const MOCK_QUERY_RUNNER = jest.fn().mockImplementation(function () {
  return {
    connect: jest.fn(),
    startTransaction: jest.fn(),
    release: jest.fn(),
    rollbackTransaction: jest.fn(),
    manager: {
      save: jest.fn(),
    },
  };
});

export const MOCK_QUERY_RUNNER_FOR_CREATE_USER = jest
  .fn()
  .mockImplementation(function () {
    return {
      connect: jest.fn(),
      startTransaction: jest.fn(),
      release: jest.fn(),
      rollbackTransaction: jest.fn(),
      manager: {
        save: jest
          .fn()
          .mockReturnValueOnce(MOCK_EXISTING_USER)
          .mockReturnValueOnce(MOCK_USER),
      },
    };
  });
