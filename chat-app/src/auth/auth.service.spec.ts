import { JwtModule } from '@nestjs/jwt';
import { PassportModule } from '@nestjs/passport';
import { Test, TestingModule } from '@nestjs/testing';
import { EmailService } from 'src/email/email.service';
import { User } from 'src/users/entities/user.entity';
import { UsersService } from 'src/users/users.service';
import { DataSource } from 'typeorm';
import { AuthService } from './auth.service';

import jwt_decode from 'jwt-decode';
import { MOCK_USER } from './mocks/data';
import { MOCK_QUERY_RUNNER_FOR_CREATE_USER } from './mocks/objects';

describe('AuthService', () => {
  let service: AuthService;
  let usersService: UsersService;

  beforeAll(async () => {
    await initTestModule();
  });

  afterEach(() => {
    jest.resetAllMocks();
  });

  describe('AuthService.validateUser', () => {
    it('should return user with correct password', async () => {
      jest.spyOn(usersService, 'findOneByEmailForAuth').mockResolvedValue({
        password: await service['hashPassword']('password'),
        ...(MOCK_USER as User),
      } as User);

      const user = await service.validateUser('test@test.com', 'password');
      expect(user).toEqual(MOCK_USER);
    });

    it('should return null with incorrect password', async () => {
      jest.spyOn(usersService, 'findOneByEmailForAuth').mockResolvedValue({
        password: await service['hashPassword']('password'),
        ...(MOCK_USER as User),
      } as User);

      const user = await service.validateUser('test@test.com', 'passwordzzz');
      expect(user).toBe(null);
    });
  });

  describe('AuthService.login', () => {
    it('should take payload and return an access_token', async () => {
      const result = await service.login(MOCK_USER as User);
      const decoded = jwt_decode<{
        username: string;
        email: string;
        iat: number;
        exp: number;
      }>(result.access_token);
      expect(decoded.username).toBe(MOCK_USER.username);
      expect(decoded.email).toBe(MOCK_USER.email);
      expect(decoded.exp - decoded.iat).toBe(60);
    });
  });

  const initTestModule = async () => {
    const module: TestingModule = await Test.createTestingModule({
      imports: [
        PassportModule,
        JwtModule.register({
          secret: 'asdf',
          secretOrPrivateKey: 'asdf',
          signOptions: {
            expiresIn: '60s',
          },
        }),
      ],
      providers: [AuthService],
    })
      .useMocker((token) => {
        if (token === UsersService) {
          return {
            findOneByEmailForAuth: jest.fn(),
            add: jest.fn(),
            update: jest.fn(),
          };
        }
        if (token === DataSource) {
          return {
            createQueryRunner: MOCK_QUERY_RUNNER_FOR_CREATE_USER,
          };
        }
        if (token === EmailService) {
          return {
            sendConfirmEmail: jest.fn(),
          };
        }
      })
      .compile();

    service = module.get<AuthService>(AuthService);
    usersService = module.get<UsersService>(UsersService);
  };
});
