import { HttpException } from '@nestjs/common';
import { JwtModule } from '@nestjs/jwt';
import { PassportModule } from '@nestjs/passport';
import { Test, TestingModule } from '@nestjs/testing';
import { getRepositoryToken } from '@nestjs/typeorm';
import { EmailService } from 'src/email/email.service';
import { RequestWithUser } from 'src/types/request';
import { User } from 'src/users/entities/user.entity';
import { UsersService } from 'src/users/users.service';
import { DataSource } from 'typeorm';
import { AuthController } from './auth.controller';
import { AuthService } from './auth.service';
import {
  MOCK_EXISTING_USER,
  MOCK_JWT_REGISTER,
  MOCK_USER_FOR_CREATE,
  MOCK_LOGIN_RESPONSE,
} from './mocks/data';
import { MOCK_QUERY_RUNNER } from './mocks/objects';

describe('AuthController', () => {
  let controller: AuthController;
  let authService: AuthService;
  let usersService: UsersService;

  beforeAll(async () => {
    await initTestModule();
  });

  afterEach(() => {
    jest.resetAllMocks();
  });

  describe('AuthController.create', () => {
    it('should handle existing user found', async () => {
      jest
        .spyOn(usersService, 'findOneByEmail')
        .mockResolvedValue(MOCK_EXISTING_USER as User);
      try {
        await controller.create(MOCK_USER_FOR_CREATE);
      } catch (error) {
        expect(error).toBeInstanceOf(HttpException);
        expect(error.status).toBe(422);
      }
    });

    it('should create user if no existing user', async () => {
      jest.spyOn(usersService, 'findOneByEmail').mockResolvedValue(null);
      jest
        .spyOn(authService, 'create')
        .mockResolvedValue(MOCK_EXISTING_USER as User);
      const result = await controller.create(MOCK_USER_FOR_CREATE);
      expect(result).toBe(
        'Registration successful. Please confirm your email.',
      );
    });
  });

  describe('AuthController.confirmEmail', () => {
    it('should throw HttpException if no token provided', async () => {
      try {
        await controller.confirmEmail(null);
      } catch (error) {
        expect(error).toBeInstanceOf(HttpException);
        expect(error.status).toBe(400);
      }
      try {
        await controller.confirmEmail('');
      } catch (error) {
        expect(error).toBeInstanceOf(HttpException);
        expect(error.status).toBe(400);
      }
    });
    it('should throw HttpException if no user is found by token', async () => {
      jest.spyOn(usersService, 'findOneByEmailToken').mockResolvedValue(null);
      try {
        await controller.confirmEmail('token');
      } catch (error) {
        expect(error).toBeInstanceOf(HttpException);
        expect(error.status).toBe(404);
      }
    });
    it('should return success after onboarding', async () => {
      jest
        .spyOn(usersService, 'findOneByEmailToken')
        .mockResolvedValue(MOCK_EXISTING_USER as User);
      jest
        .spyOn(usersService, 'update')
        .mockResolvedValue(MOCK_EXISTING_USER as User);
      const result = await controller.confirmEmail('token');
      expect(result).toBe('Email confirmed.');
    });
  });

  describe('AuthController.login', () => {
    it('should accept a req.user object', async () => {
      jest.spyOn(authService, 'login').mockResolvedValue(MOCK_LOGIN_RESPONSE);
      const reqMock = {
        user: MOCK_EXISTING_USER as User,
      } as RequestWithUser;

      const result = await controller.login(reqMock);

      expect(result).toBe(MOCK_LOGIN_RESPONSE);
    });
  });

  const initTestModule = async () => {
    const module: TestingModule = await Test.createTestingModule({
      controllers: [AuthController],
      imports: [PassportModule, JwtModule.register(MOCK_JWT_REGISTER)],
      providers: [
        AuthService,
        UsersService,
        {
          provide: getRepositoryToken(User),
          useValue: {
            findOne: jest.fn(),
            update: jest.fn(),
          },
        },
      ],
    })
      .useMocker((token) => {
        if (token === AuthService) {
          return {
            onBoardNewlyConfirmedUser: jest.fn(),
          };
        }
        if (token === UsersService) {
          return {
            findOneByEmailForAuth: jest.fn(),
            update: jest.fn(),
          };
        }
        if (token === DataSource) {
          return {
            createQueryRunner: MOCK_QUERY_RUNNER,
          };
        }
        if (token === EmailService) {
          return {
            sendConfirmEmail: jest.fn(),
          };
        }
      })
      .compile();

    controller = module.get<AuthController>(AuthController);
    authService = module.get<AuthService>(AuthService);
    usersService = module.get<UsersService>(UsersService);
  };
});
