import {
  Controller,
  Post,
  UseGuards,
  Request,
  Body,
  HttpException,
  HttpStatus,
  Get,
  Query,
  HttpCode,
} from '@nestjs/common';
import { RequestWithUser } from 'src/types/request';

import { CreateUserDto } from 'src/users/dto/create-user.dto';
import { UsersService } from 'src/users/users.service';
import { AuthService } from './auth.service';
import { LocalAuthGuard } from './guards/local-auth.guard';

@Controller('api/auth')
export class AuthController {
  constructor(
    private authService: AuthService,
    private usersService: UsersService,
  ) {}

  @UseGuards(LocalAuthGuard)
  @HttpCode(200)
  @Post('login')
  async login(@Request() req: RequestWithUser) {
    return this.authService.login(req.user);
  }

  @Post('signup')
  async create(@Body() createUserDto: CreateUserDto) {
    const existingUser = await this.usersService.findOneByEmail(
      createUserDto.email,
    );
    if (existingUser) {
      throw new HttpException(
        'Email address taken',
        HttpStatus.UNPROCESSABLE_ENTITY,
      );
    }
    await this.authService.create(createUserDto);
    return 'Registration successful. Please confirm your email.';
  }

  @Get('signup/confirm')
  async confirmEmail(@Query('token') token: string) {
    if (!token) {
      throw new HttpException('Token required', HttpStatus.BAD_REQUEST);
    }

    const user = await this.usersService.findOneByEmailToken(token);
    if (!user) {
      throw new HttpException(
        'No user found for that token.',
        HttpStatus.NOT_FOUND,
      );
    }

    await this.authService.onBoardNewlyConfirmedUser(user);

    return 'Email confirmed.';
  }
}
