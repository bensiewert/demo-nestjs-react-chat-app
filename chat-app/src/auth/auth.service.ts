import { Injectable } from '@nestjs/common';
import { JwtService } from '@nestjs/jwt';
import { DataSource } from 'typeorm';
import * as bcrypt from 'bcrypt';
import { EmailService } from 'src/email/email.service';
import { CreateUserDto } from 'src/users/dto/create-user.dto';
import { User } from 'src/users/entities/user.entity';
import { UsersService } from 'src/users/users.service';
import { generateRandomToken } from 'src/utils/random';

@Injectable()
export class AuthService {
  constructor(
    private readonly usersService: UsersService,
    private readonly jwtService: JwtService,
    private readonly dataSource: DataSource,
    private readonly emailService: EmailService,
  ) {}

  async validateUser(username: string, pass: string): Promise<Partial<User>> {
    const user = await this.usersService.findOneByEmailForAuth(username);
    if (user && (await this.verifyPassword(pass, user.password))) {
      const { password, ...result } = user;
      return result;
    }
    return null;
  }

  async login(user: User) {
    const payload = {
      email: user.email,
      sub: user.id,
      username: user.username,
    };
    return {
      access_token: this.jwtService.sign(payload),
    };
  }

  async create(createUserDto: CreateUserDto) {
    const confirmEmailToken = generateRandomToken(20);
    const user = new User();
    Object.assign(user, createUserDto);
    user.password = await this.hashPassword(createUserDto.password);
    user.confirmEmailToken = confirmEmailToken;
    await this.usersService.add(user);
    await this.emailService.sendConfirmEmail(user, confirmEmailToken);
    return user;
    // const queryRunner = this.dataSource.createQueryRunner();

    // await queryRunner.connect();
    // await queryRunner.startTransaction();
    // let newUser;
    // let confirmEmailToken;
    // try {
    //   confirmEmailToken = generateRandomToken(20);
    //   const user = await this.setNewUserForSave(
    //     createUserDto,
    //     confirmEmailToken,
    //   );
    //   newUser = await queryRunner.manager.save(user);
    //   await this.emailService.sendConfirmEmail(newUser, confirmEmailToken);
    //   await queryRunner.commitTransaction();
    // } catch (err) {
    //   // since we have errors lets rollback the changes we made
    //   await queryRunner.rollbackTransaction();
    //   throw err;
    // } finally {
    //   // you need to release a queryRunner which was manually instantiated
    //   await queryRunner.release();

    //   return newUser;
    // }
  }

  public async onBoardNewlyConfirmedUser(user: User) {
    const activatedUser = await this.usersService.update(user.id, {
      isActive: true,
      confirmEmailToken: null,
    });

    return activatedUser;
  }

  private async setNewUserForSave(
    createUserDto: CreateUserDto,
    confirmEmailToken: string,
  ) {
    const user = new User();
    Object.assign(user, createUserDto);
    user.password = await this.hashPassword(createUserDto.password);
    user.confirmEmailToken = confirmEmailToken;
    return user;
  }

  private async hashPassword(password: string) {
    const saltOrRounds = 10;
    return bcrypt.hash(password, saltOrRounds);
  }

  private verifyPassword(password: string, hash: string) {
    return bcrypt.compare(password, hash);
  }
}
