import { randomBytes } from 'crypto';

export function generateRandomToken(bytes: number): string {
  const buffer = randomBytes(bytes);
  return buffer.toString('hex');
}
