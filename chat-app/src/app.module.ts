import { Module } from '@nestjs/common';
import { TypeOrmModule } from '@nestjs/typeorm';
import { DataSource } from 'typeorm';
import { AppController } from './app.controller';
import { AppService } from './app.service';
import { UsersModule } from './users/users.module';
import { ConfigModule } from '@nestjs/config';
import { AuthModule } from './auth/auth.module';
import { BullModule } from '@nestjs/bull';
import { EventsModule } from './events/events.module';
import { PrometheusModule } from '@willsoto/nestjs-prometheus';
import {
  InMemorySessionStore,
  IN_MEMORY_SESSION_STORE_TOKEN,
} from './events/events.session-store';
import { InMemoryMessageStore } from './events/events.message-store';

@Module({
  imports: [
    PrometheusModule.register(),
    EventsModule,
    BullModule.forRoot({
      redis: {
        host: process.env.REDIS_APP_HOST,
        port: +process.env.REDIS_APP_PORT,
      },
    }),
    TypeOrmModule.forRoot({
      type: 'postgres',
      host: process.env.POSTGRES_HOST,
      port: +process.env.POSTGRES_PORT,
      username: process.env.POSTGRES_USER,
      password: process.env.POSTGRES_PASSWORD,
      database: process.env.POSTGRES_DB,
      entities: [],
      autoLoadEntities: true,
      synchronize: true,
    }),
    ConfigModule.forRoot({
      isGlobal: true,
      ignoreEnvFile: true,
    }),
    AuthModule,
    UsersModule,
  ],
  controllers: [AppController],
  providers: [AppService, InMemorySessionStore, InMemoryMessageStore],
})
export class AppModule {
  constructor(private dataSource: DataSource) {}
}
