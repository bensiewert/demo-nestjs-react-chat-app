import { IsString, MaxLength, MinLength } from 'class-validator';
import { Match } from './match.decorator';

export class CreateUserDto {
  @IsString()
  @MinLength(4)
  @MaxLength(50)
  readonly username: string;

  @IsString()
  @MinLength(4)
  @MaxLength(50)
  readonly email: string;

  @IsString()
  @MinLength(8)
  @MaxLength(100)
  readonly password: string;

  @IsString()
  @Match('password')
  readonly confirmPassword: string;
}
