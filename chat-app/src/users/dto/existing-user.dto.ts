export class ExistingUserDto {
  readonly id: string;
  readonly username: string;
  readonly email: string;
  readonly isActive: boolean;
  readonly confirmEmailToken: null | string;
  readonly password: string;
}
