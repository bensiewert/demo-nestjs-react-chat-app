import { Injectable } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { Repository } from 'typeorm';
import { UpdateUserDto } from './dto/update-user.dto';
import { User } from './entities/user.entity';

@Injectable()
export class UsersService {
  constructor(
    @InjectRepository(User)
    private readonly usersRepository: Repository<User>,
  ) {}

  add(user: User) {
    return this.usersRepository.save(user);
  }

  findAll() {
    return this.usersRepository.find();
  }

  findOne(id: string) {
    return this.usersRepository.findOneBy({ id });
  }

  findOneByEmail(email: string) {
    return this.usersRepository.findOne({
      where: { email },
    });
  }

  findOneByEmailToken(token: string) {
    return this.usersRepository.findOne({
      where: { confirmEmailToken: token },
    });
  }

  findOneByEmailForAuth(email: string) {
    return this.usersRepository.findOne({
      where: { email },
    });
  }

  async update(
    id: string,
    updateUserDto: UpdateUserDto,
  ): Promise<Partial<User>> | null {
    const result = await this.usersRepository
      .createQueryBuilder('user')
      .update(updateUserDto)
      .where('id = :id', { id })
      .returning(['id', 'username', 'email'])
      .updateEntity(true)
      .execute();

    return result.affected > 0 ? result.raw[0] : null;
  }

  remove(id: string) {
    return this.usersRepository.delete(id);
  }

  addMany(users: User[]) {
    return this.usersRepository.insert(users);
  }
}
