import { NestFactory } from '@nestjs/core';
import { NestExpressApplication } from '@nestjs/platform-express';
import { AppModule } from '../../app.module';
import { DevContextModule } from './dev-context-module';
import { DevSeed } from './dev-seed';
import { DevSeederModule } from './dev-seeder.module';

async function bootstrap() {
  const appContext = await NestFactory.createApplicationContext(
    DevContextModule,
  );
  console.log('app context opened');
  const seeder = appContext.get(DevSeed);
  await seeder.run();
  process.exitCode = 0;
}
bootstrap();
