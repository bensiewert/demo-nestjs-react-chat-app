import { Module } from '@nestjs/common';
import { UsersService } from '../../users/users.service';
import { UsersController } from '../../users/users.controller';
import { TypeOrmModule } from '@nestjs/typeorm';
import { User } from '../../users/entities/user.entity';
import { DevSeed } from './dev-seed';

@Module({
  imports: [TypeOrmModule.forFeature([User])],
  controllers: [UsersController],
  providers: [UsersService, DevSeed],
  exports: [UsersService],
})
export class DevSeederModule {}
