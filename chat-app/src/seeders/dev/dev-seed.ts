import { Injectable } from '@nestjs/common';
import { faker } from '@faker-js/faker';
import { User } from 'src/users/entities/user.entity';
import { UsersService } from 'src/users/users.service';

@Injectable()
export class DevSeed {
  constructor(private readonly usersService: UsersService) {}

  async run() {
    const users = [];
    for (let i = 0; i < 5; i++) {
      const user = new User();
      user.username = faker.internet.userName();
      user.email = faker.internet.email();
      user.password = 'password';
      users.push(user);
    }

    try {
      await this.usersService.addMany(users);
      console.log('done');
    } catch (error) {
      console.log('error: ', error);
      process.exit();
    }
  }
}
