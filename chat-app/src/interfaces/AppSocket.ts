import { Socket } from 'socket.io';

export type CustomAppSocket = {
  userId: string;
  sessionId: string;
  username: string;
};

export type AppSocket = Socket & CustomAppSocket;
