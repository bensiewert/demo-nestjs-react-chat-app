import { Injectable } from '@nestjs/common';

export const IN_MEMORY_MESSAGE_STORE_TOKEN = Symbol(
  'IN_MEMORY_MESSAGE_STORE_TOKEN',
);

interface Message {
  content: string;
  from: string;
  to: string;
}

@Injectable()
export class InMemoryMessageStore {
  messages: Message[];
  constructor() {
    this.messages = [];
  }

  saveMessage(message: Message) {
    this.messages.push(message);
  }

  findMessagesForUser(userId: string) {
    return this.messages.filter(
      ({ from, to }) => from === userId || to === userId,
    );
  }
}
