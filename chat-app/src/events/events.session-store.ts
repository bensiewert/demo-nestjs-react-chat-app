import { Injectable } from '@nestjs/common';

export const IN_MEMORY_SESSION_STORE_TOKEN = Symbol(
  'IN_MEMORY_SESSION_STORE_TOKEN',
);

interface Session {
  userId: string;
  username: string;
  connected: boolean;
  sessionId: string;
}

@Injectable()
export class InMemorySessionStore {
  sessions: Map<string, Session>;
  constructor() {
    this.sessions = new Map<string, Session>();
  }

  findSession(id: string) {
    return this.sessions.get(id);
  }

  saveSession(id: string, session: Session) {
    // TODO: setup redis to store sessions
    // temporary brute force method to prevent duplicate sessions
    for (const [key, value] of this.sessions.entries()) {
      if (value.userId === session.userId) {
        this.sessions.delete(key);
      }
    }

    this.sessions.set(id, session);
  }

  findAllSessions() {
    return [...this.sessions.values()];
  }

  deleteSession(id: string) {
    this.sessions.delete(id);
  }
}
