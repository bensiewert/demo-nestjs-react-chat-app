import {
  SubscribeMessage,
  WebSocketGateway,
  WebSocketServer,
  OnGatewayConnection,
  OnGatewayDisconnect,
} from '@nestjs/websockets';

import { Server } from 'socket.io';
import { AppSocket } from 'src/interfaces/AppSocket';
import { UsersService } from 'src/users/users.service';
import { generateRandomToken } from 'src/utils/random';
import { InMemoryMessageStore } from './events.message-store';
import { InMemorySessionStore } from './events.session-store';

@WebSocketGateway({
  cors: {
    origin: '*',
  },
})
export class EventsGateway implements OnGatewayConnection, OnGatewayDisconnect {
  @WebSocketServer()
  server: Server;

  constructor(
    private readonly usersService: UsersService,
    // TODO: swap this with redis
    private readonly sessionStore: InMemorySessionStore,
    private readonly messageStore: InMemoryMessageStore,
  ) {}

  async handleConnection(socket: AppSocket) {
    this._handleIncomingSession(socket);
    await this._attachUserDetails(socket);
    this._completeConnectSession(socket);
    this._getAndEmitUsers(socket);
  }

  async handleDisconnect(socket: AppSocket) {
    const matchingSockets = await this.server.in(socket.userId).allSockets();
    const isDisconnected = matchingSockets.size === 0;
    if (isDisconnected) {
      // notify other users
      socket.broadcast.emit('user disconnected', socket.userId);
      this.sessionStore.saveSession(socket.sessionId, {
        userId: socket.userId,
        username: socket.username,
        connected: false,
        sessionId: socket.sessionId,
      });
    }
  }

  @SubscribeMessage('private message')
  privateMessage(socket: AppSocket, { content, to }) {
    const message = {
      content,
      from: socket.userId,
      to,
      timestamp: Date.now(),
      read: false,
    };

    socket.to(to).to(socket.userId).emit('private message', message);

    this.messageStore.saveMessage(message);
  }

  private _handleIncomingSession(socket: AppSocket): void {
    const sessionId = socket.handshake.auth.sessionId;
    if (sessionId && sessionId !== 'undefined') {
      const session = this.sessionStore.findSession(sessionId);
      if (session) {
        socket.sessionId = sessionId;
      } else {
        socket.sessionId = generateRandomToken(8);
      }
    } else {
      socket.sessionId = generateRandomToken(8);
    }
  }

  private async _attachUserDetails(socket: AppSocket): Promise<void> {
    const userdata = await this.usersService.findOne(socket.userId);

    if (!userdata) {
      // TODO setup logger to log this event
      return;
    }

    socket.username = userdata.username;
  }

  private _completeConnectSession(socket: AppSocket): void {
    const sessionData = {
      userId: socket.userId,
      username: socket.username,
      connected: true,
      sessionId: socket.sessionId,
    };

    this.sessionStore.saveSession(socket.sessionId, sessionData);
    socket.emit('session', sessionData);
    socket.join(socket.userId);
  }

  private _getAndEmitUsers(socket: AppSocket): void {
    const users = [];
    const messagesPerUser = new Map();
    this.messageStore.findMessagesForUser(socket.userId).forEach((message) => {
      const { from, to } = message;
      const otherUser = socket.userId === from ? to : from;
      if (messagesPerUser.has(otherUser)) {
        messagesPerUser.get(otherUser).push(message);
      } else {
        messagesPerUser.set(otherUser, [message]);
      }
    });

    this.sessionStore.findAllSessions().forEach((session) => {
      console.log('session: ', session.sessionId, session.username);
      users.push({
        userId: session.userId,
        username: session.username,
        connected: session.connected,
        sessionId: session.sessionId,
        messages: messagesPerUser.get(session.userId) || [],
      });
    });

    socket.emit('users', users);
    socket.broadcast.emit('user connected', {
      userId: socket.userId,
      username: socket.username,
      connected: true,
      sessionId: socket.sessionId,
      messages: [],
    });
  }
}
