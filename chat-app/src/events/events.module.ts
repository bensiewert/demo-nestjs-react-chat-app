import { Module } from '@nestjs/common';
import { UsersModule } from 'src/users/users.module';
import { EventsGateway } from './events.gateway.socketio';
import { InMemoryMessageStore } from './events.message-store';
import { InMemorySessionStore } from './events.session-store';

@Module({
  imports: [UsersModule],
  providers: [EventsGateway, InMemorySessionStore, InMemoryMessageStore],
})
export class EventsModule {}
