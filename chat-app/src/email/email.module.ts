import { MailerModule } from '@nestjs-modules/mailer';
import { HandlebarsAdapter } from '@nestjs-modules/mailer/dist/adapters/handlebars.adapter';
import { Global, Module } from '@nestjs/common';
import { EmailService } from './email.service';
import { join } from 'path';
import { ConfigModule, ConfigService } from '@nestjs/config';

@Global()
@Module({
  imports: [
    MailerModule.forRootAsync({
      // imports: [ConfigModule], // import module if not enabled globally
      useFactory: async (config: ConfigService) => ({
        // transport: config.get("MAIL_TRANSPORT"),
        // or
        transport: {
          // host: config.get('MAIL_HOST'),
          host: 'smtp.mailtrap.io',

          secure: false,
          auth: {
            // user: config.get('MAIL_USER'),
            user: '2e991ba8752f01',
            // pass: config.get('MAIL_PASSWORD'),
            pass: 'd68114c0899e32',
          },
        },
        defaults: {
          // from: `"No Reply" <${config.get('MAIL_FROM')}>`,
          from: `"No Reply" <ben@nest-lms.com>`,
        },
        // preview: true,
        template: {
          dir: __dirname + '/templates',
          adapter: new HandlebarsAdapter(),
          options: {
            strict: true,
          },
        },
      }),
      inject: [ConfigService],
    }),
  ],
  providers: [EmailService],
  exports: [EmailService],
})
export class EmailModule {}
