import { ISendMailOptions, MailerService } from '@nestjs-modules/mailer';
import { Injectable } from '@nestjs/common';
import { ConfigService } from '@nestjs/config';
import { EnvVariables } from 'src/config';
import { User } from 'src/users/entities/user.entity';

@Injectable()
export class EmailService {
  constructor(
    private mailerService: MailerService,
    private configService: ConfigService,
  ) {}

  sendEmail(sendMailOptions: ISendMailOptions) {
    return this.mailerService.sendMail(sendMailOptions);
  }

  sendConfirmEmail(user: User, token: string) {
    const url = `${this.configService.get<string>(
      EnvVariables.APP_URL,
    )}/auth/confirm?token=${token}`;

    return this.mailerService.sendMail({
      to: user.email,
      subject: 'Confirm your email address',
      template: './confirm_email',
      context: {
        appName: this.configService.get<string>(EnvVariables.APP_NAME),
        username: user.username,
        url,
        description:
          'Please confirm your email address by clicking the button below:',
        buttonText: 'Confirm email address',
      },
    });
  }

  sendInviteEmail(to: string, token: string) {
    const url = `${this.configService.get<string>(
      EnvVariables.APP_URL,
    )}/auth/invite?token=${token}`;

    return this.mailerService.sendMail({
      to: to,
      subject: 'You have been invited to join App',
      template: './confirm_email',
      context: {
        appName: this.configService.get<string>(EnvVariables.APP_NAME),
        name: 'test',
        url,
        description: 'Please accept your invite by clicking the button below:',
        buttonText: 'Accept invite',
      },
    });
  }
}
