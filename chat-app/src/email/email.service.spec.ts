import { MailerService, MAILER_OPTIONS } from '@nestjs-modules/mailer';
import { ConfigModule } from '@nestjs/config';
import { Test, TestingModule } from '@nestjs/testing';
import { EmailService } from './email.service';

describe('EmailService', () => {
  let service: EmailService;

  beforeAll(async () => {
    await initTestModule();
  });

  describe('EmailService defined', () => {
    it('should be defined', () => {
      expect(service).toBeDefined();
    });
  });

  const initTestModule = async () => {
    const module: TestingModule = await Test.createTestingModule({
      imports: [ConfigModule],
      providers: [
        EmailService,
        MailerService,
        {
          provide: MAILER_OPTIONS,
          useValue: {
            transport: {
              host: 'host',
              secure: 'false',
            },
          },
        },
      ],
    })
      .useMocker((token) => {
        if (token === EmailService) {
          return {
            sendConfirmEmail: jest.fn(),
          };
        }
      })
      .compile();

    service = module.get<EmailService>(EmailService);
  };
});
