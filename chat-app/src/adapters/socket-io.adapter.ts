import { IoAdapter } from '@nestjs/platform-socket.io';
import { ServerOptions, Server } from 'socket.io';

import { AppSocket } from 'src/interfaces/AppSocket';
import { JwtService } from '@nestjs/jwt';
import { INestApplicationContext } from '@nestjs/common';

export class SocketIoAdapter extends IoAdapter {
  constructor(private readonly app: INestApplicationContext) {
    super(app);
  }

  createIOServer(port: number, options?: ServerOptions): Server {
    const server: Server = super.createIOServer(port, options);
    const jwtService = this.app.get(JwtService);

    server.use(createTokenMiddleware(jwtService));

    return server;
  }
}

const createTokenMiddleware =
  (jwtService: JwtService) => (socket: AppSocket, next) => {
    const token =
      socket.handshake.auth.token || socket.handshake.headers['token'];

    try {
      const payload = jwtService.verify(token);
      socket.userId = payload.sub;
      next();
    } catch {
      next(new Error('FORBIDDEN'));
    }
  };
