import React from 'react';
import { useRouter } from 'next/router';
import Paper from '@mui/material/Paper';
import Typography from '@mui/material/Typography';

import { useAuth } from 'src/contexts/AuthContext';

export default function ConfirmPage() {
  const router = useRouter();
  const { confirmAccount } = useAuth();
  const urlParams = new URLSearchParams(window.location.search);
  const token = urlParams.get('token');
  const [loading, setLoading] = React.useState(true);
  const [message, setMessage] = React.useState('');

  React.useEffect(() => {
    const initConfirmPage = async () => {
      if (!token) {
        router.push('/login');
      }
      const r = await confirmAccount(token as string);
      if (r.success) {
        setMessage('Email confirmed, Please login.');
      } else {
        setMessage(r.reason);
      }
      setLoading(false);
    };
    initConfirmPage();
  }, []);

  if (loading) {
    return <p>Loading...</p>;
  }

  return (
    <Paper style={{ padding: 20 }}>
      <Typography variant="subtitle1" color="inherit">
        {message}
      </Typography>
    </Paper>
  );
}
