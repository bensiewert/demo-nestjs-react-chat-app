import React from 'react';
import Paper from '@mui/material/Paper';
import TextField from '@mui/material/TextField';
import Button from '@mui/material/Button';
import CircularProgress from '@mui/material/CircularProgress';
import Typography from '@mui/material/Typography';
import { Formik, FormikProps } from 'formik';
import { string as yupString, object as yupObject } from 'yup';
import { useAuth } from 'src/contexts/AuthContext';
import Link from 'src/components/Link';
import { ICredentials } from 'src/interfaces/Auth';
import { Divider } from '@mui/material';

const validationSchema = yupObject({
  email: yupString().email('Enter a valid email').required('Email is required'),
  password: yupString().required('Password is required'),
});

export default function LoginPage() {
  const { login, setErrorMessage } = useAuth();

  React.useEffect(() => {
    setErrorMessage(null);
  }, []);

  const handleSubmit = (values: ICredentials) => {
    setErrorMessage(null);
    login(values);
  };

  const initialValues: ICredentials = {
    email: '',
    password: '',
  };

  return (
    <Paper style={{ padding: 20 }}>
      <Formik
        initialValues={initialValues}
        validationSchema={validationSchema}
        onSubmit={(values: ICredentials) => handleSubmit(values)}
      >
        {(formProps: FormikProps<ICredentials>) => (
          <SignupForm {...formProps} />
        )}
      </Formik>
    </Paper>
  );
}

function SignupForm(props: FormikProps<ICredentials>) {
  const { loading, errorMessage, formStep } = useAuth();

  const {
    values: { email, password },
    errors,
    handleSubmit,
    handleChange,
    isValid,
  } = props;

  if (formStep > 0) {
    return (
      <div style={{ paddingTop: 20, paddingBottom: 20 }}>
        <Typography variant="subtitle1" color="inherit">
          An email was sent to <strong>{email}</strong>
        </Typography>
        <Typography variant="subtitle1" color="inherit">
          Check your email for a message from us.
        </Typography>
        <Typography variant="subtitle1" color="inherit">
          <Link href="/login">Login</Link>
        </Typography>
      </div>
    );
  }

  return (
    <form onSubmit={handleSubmit}>
      <h2>Signin</h2>
      {errorMessage ? (
        <Typography variant="caption" color="error">
          {errorMessage}
        </Typography>
      ) : null}

      <TextField
        name="email"
        autoComplete="off"
        type="email"
        disabled={loading}
        helperText={errors.email}
        error={Boolean(errors.email)}
        label="Email"
        fullWidth
        value={email}
        onChange={handleChange}
        style={{ marginTop: 20 }}
        variant="outlined"
      />
      <TextField
        name="password"
        autoComplete="off"
        type="password"
        disabled={loading}
        helperText={errors.password}
        error={Boolean(errors.password)}
        label="Password"
        fullWidth
        value={password}
        onChange={handleChange}
        style={{ marginTop: 20 }}
        variant="outlined"
      />

      <div
        style={{
          display: 'flex',
          justifyContent: 'center',
        }}
      >
        <Button
          type="submit"
          variant="outlined"
          color="primary"
          disabled={!isValid || loading}
          style={{ marginTop: 40, width: '50%' }}
          startIcon={
            loading ? <CircularProgress size={24} color="inherit" /> : null
          }
          fullWidth
        >
          Login
        </Button>
      </div>

      <Divider style={{ marginTop: 40 }} />
      <p style={{ marginTop: 20, textAlign: 'center' }}>
        <Link href="/reset-pw">Can&apos;t log In?</Link>
      </p>
    </form>
  );
}
