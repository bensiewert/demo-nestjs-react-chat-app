import React from 'react';
import Paper from '@mui/material/Paper';
import TextField from '@mui/material/TextField';
import Button from '@mui/material/Button';
import CircularProgress from '@mui/material/CircularProgress';
import Typography from '@mui/material/Typography';
import { Formik, FormikProps } from 'formik';
import { string as yupString, object as yupObject, ref as yupRef } from 'yup';
import { useAuth } from 'src/contexts/AuthContext';
import Link from 'src/components/Link';
import { IRegisterParams } from 'src/interfaces/Auth';

const validationSchema = yupObject({
  email: yupString().email('Enter a valid email').required('Email is required'),
  username: yupString().min(4).max(50).required('Username is required'),
  password: yupString()
    .required('Enter a valid password')
    .matches(
      /^(?!password)/i,
      'Cannot contain the word "password" in password',
    ),
  confirmPassword: yupString().oneOf(
    [yupRef('password'), null],
    'Passwords must match',
  ),
});

export default function RegisterPage() {
  const { register, setFormStep, setErrorMessage } = useAuth();

  React.useEffect(() => {
    setErrorMessage(null);
    setFormStep(0);
  }, []);

  const handleSubmit = (values: IRegisterParams) => {
    setErrorMessage(null);
    register(values);
  };

  const initialValues: IRegisterParams = {
    username: '',
    email: '',
    password: '',
    confirmPassword: '',
  };

  return (
    <Paper style={{ padding: 20 }}>
      <Formik
        initialValues={initialValues}
        validationSchema={validationSchema}
        onSubmit={(values: IRegisterParams) => handleSubmit(values)}
      >
        {(formProps: FormikProps<IRegisterParams>) => (
          <SignupForm {...formProps} />
        )}
      </Formik>
    </Paper>
  );
}

function SignupForm(props: FormikProps<IRegisterParams>) {
  const { loading, errorMessage, formStep } = useAuth();

  const {
    values: { username, email, password, confirmPassword },
    errors,
    handleSubmit,
    handleChange,
    isValid,
  } = props;

  if (formStep > 0) {
    return (
      <div style={{ paddingTop: 20, paddingBottom: 20 }}>
        <Typography variant="subtitle1" color="inherit">
          An email was sent to <strong>{email}</strong>
        </Typography>
        <Typography variant="subtitle1" color="inherit">
          Check your email for a message from us.
        </Typography>
        <Typography variant="subtitle1" color="inherit">
          <Link href="/login">Login</Link>
        </Typography>
      </div>
    );
  }

  return (
    <form onSubmit={handleSubmit}>
      <h2>Signup</h2>
      {errorMessage ? (
        <Typography variant="caption" color="error">
          {errorMessage}
        </Typography>
      ) : null}

      <TextField
        name="username"
        autoComplete="off"
        type="text"
        disabled={loading}
        helperText={errors.username}
        error={Boolean(errors.username)}
        label="Username"
        fullWidth
        value={username}
        onChange={handleChange}
        style={{ marginTop: 20 }}
        variant="outlined"
      />

      <TextField
        name="email"
        autoComplete="off"
        type="email"
        disabled={loading}
        helperText={errors.email}
        error={Boolean(errors.email)}
        label="Email"
        fullWidth
        value={email}
        onChange={handleChange}
        style={{ marginTop: 20 }}
        variant="outlined"
      />
      <TextField
        name="password"
        autoComplete="off"
        type="password"
        disabled={loading}
        helperText={errors.password}
        error={Boolean(errors.password)}
        label="Password"
        fullWidth
        value={password}
        onChange={handleChange}
        style={{ marginTop: 20 }}
        variant="outlined"
      />
      <TextField
        name="confirmPassword"
        autoComplete="off"
        type="password"
        disabled={loading}
        helperText={errors.confirmPassword}
        error={Boolean(errors.confirmPassword)}
        label="Confirm Password"
        fullWidth
        value={confirmPassword}
        onChange={handleChange}
        style={{ marginTop: 20 }}
        variant="outlined"
      />

      <div
        style={{
          display: 'flex',
          justifyContent: 'center',
        }}
      >
        <Button
          type="submit"
          variant="outlined"
          color="primary"
          disabled={!isValid || loading}
          style={{ marginTop: 40, width: '50%' }}
          startIcon={
            loading ? <CircularProgress size={24} color="inherit" /> : null
          }
          fullWidth
        >
          Signup
        </Button>
      </div>
    </form>
  );
}
