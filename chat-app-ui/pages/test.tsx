import React from 'react';

import { AppBar, Toolbar, Typography } from '@mui/material';
import SideChatBar from '../src/components/SideChatBar';

const Layout = () => {
  return (
    <div
      style={{
        display: 'flex',
      }}
    >
      <AppBar
        position="fixed"
        style={{
          zIndex: 100 + 1,
        }}
      >
        <Toolbar>
          <Typography variant="h6" noWrap>
            Whatsapp Layout
          </Typography>
        </Toolbar>
      </AppBar>
      <SideChatBar />
      <main
        style={{
          flexGrow: 1,
          padding: 3,
        }}
      >
        <div />
      </main>
    </div>
  );
};

export default Layout;
