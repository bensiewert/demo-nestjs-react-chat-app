import { styled } from '@mui/material';
import Image from 'next/image';
import React, { useState } from 'react';
import { User } from 'src/api/models/user';
import { useChat } from 'src/contexts/ChatContext';
import { useSocketIO } from 'src/contexts/SocketIoContext';
import { ChatUser } from 'src/interfaces/ChatUser';
import ChatBar from '../src/components/ChatBar';
import ChatHeader from '../src/components/ChatHeader';
import ChatMessages from '../src/components/ChatMessages';
import SideChatBar from '../src/components/SideChatBar';
import SideNavigationBar from '../src/components/SideNavigationBar';

const StyledChatSection = styled('div')(({ theme }) => ({
  display: 'flex',
  flexDirection: 'column',
  height: '100vh',
  overflowY: 'auto',
  width: '100%',
  backgroundColor: theme.palette.surfaces.dark,
}));

export default function HomePage() {
  const { emitChatMessage } = useSocketIO();
  const { chatUsers, chatUser } = useChat();
  const [chatText, setChatText] = useState<string>('');

  const handleTextInput = (
    event: React.ChangeEvent<HTMLInputElement>,
  ): void => {
    setChatText(event.target.value);
  };

  const handleSendChatText = (user: ChatUser | undefined) => {
    if (user) {
      sendChatText(user);
    }
  };

  const handleOnEnterKeyPress = (
    event: React.KeyboardEvent<HTMLInputElement | HTMLTextAreaElement>,
    user: ChatUser | undefined,
  ) => {
    if (event.key === 'Enter' && user) {
      sendChatText(user);
    }
  };

  const sendChatText = (user: User) => {
    setChatText('');
    if (chatText.length > 0) {
      emitChatMessage(user, chatText);
    }
  };

  return (
    <div style={{ display: 'flex' }}>
      <SideNavigationBar />
      <SideChatBar users={chatUsers} />
      <StyledChatSection>
        <ChatHeader />
        <ChatMessages messages={chatUser?.messages} />

        <ChatBar
          chatText={chatText}
          handleTextInput={handleTextInput}
          handleOnEnterKeyPress={handleOnEnterKeyPress}
          handleSendChatText={handleSendChatText}
        />

        <div
          style={{
            height: '100%',
            width: '100%',
            position: 'fixed',
          }}
        >
          <Image
            style={{ objectFit: 'cover' }}
            src="/images/topography.svg"
            fill
            alt="background"
          />
        </div>
      </StyledChatSection>
    </div>
  );
}
