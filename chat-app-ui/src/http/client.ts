import axios, { AxiosError, AxiosRequestConfig, AxiosResponse } from 'axios';
import { API_URL } from 'src/config';
import { getAccessToken } from 'src/utils/local-storage';

export interface AppErrorResponse {
  message: string;
}

export function httpClient() {
  const instance = axios.create({
    baseURL: API_URL,
    timeout: 60000,
  });

  instance.interceptors.request.use((config) => {
    if (config.url?.includes('/auth')) {
      return config;
    }
    config.headers.Authorization = `Bearer ${getAccessToken()}`;
    return config;
  });

  instance.interceptors.response.use(
    (response) => {
      return response;
    },
    (error) => {
      const e = error as AxiosError;
      if (e.isAxiosError) {
        return Promise.reject<AxiosError<AppErrorResponse>>(error);
      }

      // todo: handle global errors that require 500 page (app is unusable)
      return Promise.reject(error);
    },
  );
  process.env.API_URL;
  return instance;
}

export function buildParams(
  url: string,
  params: AxiosRequestConfig | undefined,
) {
  const paramsBase: AxiosRequestConfig = {
    headers: {
      'Content-Type': 'application/json',
    },
    method: 'GET',
    data: null,
    url,
  };
  return Object.assign({}, paramsBase, params);
}

export function request<T>(
  url: string,
  params?: AxiosRequestConfig,
): Promise<AxiosResponse<T>> {
  const config = buildParams(url, params);
  const client = httpClient();
  return client.request(config);
}
