import { List } from '@mui/material';
import { Container } from '@mui/system';
import React from 'react';
import { ChatMessage } from '../interfaces/ChatMessage';
import AlwaysScrollToBottom from './AlwaysScrollToBottom';
import ChatBubble from './ChatBubble';

interface Props {
  messages: ChatMessage[] | undefined;
}

export default function ChatMessages(props: Props) {
  const { messages } = props;
  return (
    <div style={{ flex: 1, height: '100%', overflowY: 'auto', zIndex: 1 }}>
      <Container>
        <List>
          {messages &&
            messages.map((message, i) => (
              <ChatBubble key={i} message={message} />
            ))}
        </List>
        <AlwaysScrollToBottom />
      </Container>
    </div>
  );
}
