import React from 'react';
import type { ReactNode } from 'react';

export default function Page({ children }: { children: ReactNode }) {
  return (
    <div style={{ display: 'flex', flexDirection: 'column', height: '100vh' }}>
      {children}
    </div>
  );
}
