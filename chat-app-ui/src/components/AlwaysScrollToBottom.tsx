import React, { useEffect, useRef } from 'react';

export default function AlwaysScrollToBottom() {
  const elementRef = useRef<HTMLElement>(null);
  useEffect(() => elementRef.current?.scrollIntoView());
  return <div ref={elementRef as React.RefObject<HTMLDivElement>} />;
}
