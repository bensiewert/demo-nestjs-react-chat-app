import { alpha, Avatar, styled, Typography } from '@mui/material';
import moment from 'moment';
import React from 'react';

const chatsList = [
  {
    avatar: '',
    lastMessageTime: moment().format('h:mm a'),
    lastMessageUser: 'Ryan',
    lastMessagePreview: 'Suhh ',
    chatName: 'Devs',
  },
  {
    avatar: '',
    lastMessageTime: moment().format('h:mm a'),
    lastMessageUser: 'Kevin',
    lastMessagePreview: 'Hey what is up, wanna go to the disko... ',
    chatName: 'Brohs',
  },
];

const ChatItemWrapper = styled('div')(({ theme }) => ({
  display: 'flex',
  justifyContent: 'center',
  alignItems: 'center',
  margin: '10px 0',
  padding: '5px 0',
  cursor: 'pointer',
  borderRadius: 5,
  '&:hover': {
    backgroundColor: alpha(theme.palette.common.white, 0.25),
  },
}));

export default function ChatsList() {
  return (
    <div style={{ margin: '0 5px' }}>
      {chatsList.map((chat, i) => {
        return (
          <ChatItemWrapper key={i}>
            <div style={{ padding: 10 }}>
              <Avatar />
            </div>

            <div style={{ width: '100%', paddingRight: 20 }}>
              <div style={{ display: 'flex', justifyContent: 'space-between' }}>
                <Typography fontSize={14} fontWeight={900}>
                  {chat.chatName}
                </Typography>
                <Typography fontSize={12}>{chat.lastMessageTime}</Typography>
              </div>
              <div style={{ display: 'flex', justifyContent: 'space-between' }}>
                <Typography fontSize={12}>
                  {chat.lastMessageUser}: {chat.lastMessagePreview}
                </Typography>
              </div>
            </div>
          </ChatItemWrapper>
        );
      })}
    </div>
  );
}
