import React from 'react';
import * as yup from 'yup';
import Button from '@mui/material/Button';
import TextField from '@mui/material/TextField';
import DialogActions from '@mui/material/DialogActions';
import DialogContent from '@mui/material/DialogContent';
import DialogTitle from '@mui/material/DialogTitle';
import LoadingButton from 'src/components/LoadingButton';
import { Formik, FormikProps } from 'formik';

interface Props {
  handleClose: () => void;
  setOpen: (value: React.SetStateAction<boolean>) => void;
}

export default function NewChatForm(props: Props) {
  // eslint-disable-next-line @typescript-eslint/no-unused-vars
  const { handleClose, setOpen } = props;
  const [loading, setLoading] = React.useState(false);
  // eslint-disable-next-line @typescript-eslint/no-unused-vars
  const [initialValues, setInitialValues] = React.useState({
    name: '',
  });

  const validationSchema = yup.object({
    name: yup.string().required('Enter a name'),
  });

  // eslint-disable-next-line @typescript-eslint/no-unused-vars, @typescript-eslint/no-explicit-any
  const handleSubmit = async (values: any) => {
    setLoading(true);
  };
  return (
    <Formik
      initialValues={initialValues}
      validationSchema={validationSchema}
      // eslint-disable-next-line @typescript-eslint/no-explicit-any
      onSubmit={(values: any) => handleSubmit(values)}
    >
      {/* eslint-disable-next-line @typescript-eslint/no-explicit-any */}
      {(formProps: FormikProps<any>) => (
        <>
          <DialogTitle>New Chat</DialogTitle>

          <DialogContent>
            <form onSubmit={formProps.handleSubmit}>
              <TextField
                name="name"
                autoComplete="off"
                type="text"
                // helperText={formProps.errors.name}
                error={Boolean(formProps.errors.name)}
                label="Name"
                fullWidth
                value={formProps.values.name}
                onChange={formProps.handleChange}
                style={{ marginTop: 20 }}
              />
              <TextField
                name="number"
                autoComplete="off"
                type="text"
                // helperText={formProps.errors.number}
                error={Boolean(formProps.errors.number)}
                label="Number"
                fullWidth
                value={formProps.values.number}
                onChange={formProps.handleChange}
                style={{ marginTop: 20 }}
              />
              <DialogActions>
                <Button onClick={handleClose}>Cancel</Button>
                <LoadingButton
                  type="submit"
                  buttonText="Save"
                  loading={loading}
                />
              </DialogActions>
            </form>
          </DialogContent>
        </>
      )}
    </Formik>
  );
}
