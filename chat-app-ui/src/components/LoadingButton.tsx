import React from 'react';
import { Button, ButtonProps, CircularProgress } from '@mui/material';

interface Props extends ButtonProps {
  loading: boolean;
  wrapperStyle?: React.CSSProperties | undefined;
  buttonText: string;
}

export default function LoadingButton(props: Props) {
  const {
    loading,
    type,
    variant,
    color,
    style,
    wrapperStyle,
    onClick,
    buttonText,
  } = props;

  return (
    <div style={wrapperStyle}>
      <Button
        onClick={onClick}
        type={type}
        variant={variant}
        color={color}
        disabled={loading}
        style={style}
        startIcon={
          loading ? <CircularProgress size={24} color="inherit" /> : null
        }
      >
        {buttonText}
      </Button>
    </div>
  );
}
