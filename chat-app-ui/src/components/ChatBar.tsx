import React from 'react';
import { Divider, IconButton, InputBase, Paper, styled } from '@mui/material';
import SendIcon from '@mui/icons-material/Send';
import { useChat } from 'src/contexts/ChatContext';
import { ChatUser } from 'src/interfaces/ChatUser';

const StyledChatBar = styled(Paper)(() => ({
  p: '2px 4px',
  display: 'flex',
  alignItems: 'center',
  zIndex: 1,
}));

interface Props {
  chatText: string;
  handleTextInput: (event: React.ChangeEvent<HTMLInputElement>) => void;
  handleSendChatText: (user: ChatUser | undefined, message: string) => void;
  handleOnEnterKeyPress: (
    event: React.KeyboardEvent<HTMLInputElement | HTMLTextAreaElement>,
    user: ChatUser | undefined,
  ) => void;
}

export default function ChatBar(props: Props) {
  const {
    chatText,
    handleTextInput,
    handleOnEnterKeyPress,
    handleSendChatText,
  } = props;

  const { chatUser } = useChat();

  return (
    <StyledChatBar>
      <InputBase
        fullWidth
        sx={{ ml: 1, flex: 1 }}
        placeholder="Chat"
        inputProps={{ 'aria-label': 'chat' }}
        value={chatText}
        onChange={(event: React.ChangeEvent<HTMLInputElement>) =>
          handleTextInput(event)
        }
        onKeyDown={(
          event: React.KeyboardEvent<HTMLInputElement | HTMLTextAreaElement>,
        ) => handleOnEnterKeyPress(event, chatUser)}
      />
      <Divider sx={{ height: 28, m: 0.5 }} orientation="vertical" />
      <IconButton
        color="primary"
        sx={{ p: '10px' }}
        aria-label="directions"
        onClick={() => handleSendChatText(chatUser, chatText)}
      >
        <SendIcon />
      </IconButton>
    </StyledChatBar>
  );
}
