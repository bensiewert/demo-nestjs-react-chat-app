import { Avatar, IconButton, Typography } from '@mui/material';
import SearchIcon from '@mui/icons-material/Search';
import React from 'react';
import { useChat } from 'src/contexts/ChatContext';

export default function ChatHeader() {
  const { chatUser } = useChat();

  return (
    <div
      style={{
        display: 'flex',
        justifyContent: 'space-between',
        alignItems: 'center',
        padding: '10px 20px',
        backdropFilter: 'blur(6px)',
        zIndex: 1,
      }}
    >
      <div style={{ display: 'flex', alignItems: 'center' }}>
        <Avatar />
        <div style={{ marginLeft: 10 }}>
          <Typography fontSize={14}>{chatUser?.username}</Typography>
          {/* <Typography fontSize={12}>ppl</Typography> */}
        </div>
      </div>

      <IconButton color="primary" sx={{ p: '10px' }} aria-label="directions">
        <SearchIcon />
      </IconButton>
    </div>
  );
}
