import { styled, Theme, Typography } from '@mui/material';
import moment from 'moment';
import React from 'react';
import { useAuth } from 'src/contexts/AuthContext';
import { ChatMessage } from '../interfaces/ChatMessage';

const StyledChatBubble = styled('div')(() => ({
  margin: '6px 0px',
  padding: '12px',
  position: 'relative',
  maxWidth: '50%',
  minWidth: '20%',
  borderRadius: '4px',
  boxShadow: '0 1px 3px rgba(0, 0, 0, 0.13)',
}));

const StyledChatBubbleLeft = styled(StyledChatBubble)(({ theme }) => ({
  background: theme.palette.surfaces.main,
  ':before': {
    content: '""',
    position: 'absolute',
    width: 0,
    height: 0,
    left: -6,
    top: 0,
    border: '10px solid transparent',
    borderTopColor: theme.palette.surfaces.main,
  },
}));

const StyledChatBubbleRight = styled(StyledChatBubble)(({ theme }) => ({
  background: theme.palette.primary.main,
  ':before': {
    content: '""',
    position: 'absolute',
    width: 0,
    height: 0,
    right: -6,
    top: 0,
    border: '10px solid transparent',
    borderTopColor: theme.palette.primary.main,
  },
}));

interface StyledCheckMarkProps {
  theme?: Theme;
  read: boolean;
}
const StyledCheckMark = styled('span')<StyledCheckMarkProps>(({ read }) => ({
  float: 'right',
  position: 'absolute',
  marginLeft: 15,
  bottom: 0,
  right: -4,
  transform: 'rotate(30deg)',
  ':before': {
    width: 2,
    height: 6,
    content: '""',
    display: 'block',
    position: 'relative',
    float: 'right',
    margin: 5,
    right: 14,
    bottom: -2,
    border: '1px solid transparent',
    borderColor: read
      ? 'transparent pink pink transparent'
      : 'transparent grey grey transparent',
  },
  ':after': {
    width: 2,
    height: 6,
    content: '""',
    display: 'block',
    position: 'relative',
    float: 'right',
    margin: 5,
    right: -1,
    bottom: 0,
    border: '1px solid transparent',
    borderColor: read
      ? 'transparent pink pink transparent'
      : 'transparent grey grey transparent',
  },
}));

interface Props {
  message: ChatMessage;
}

export default function ChatBubble(props: Props) {
  const { message } = props;
  const { user } = useAuth();

  const renderChatContent = () => (
    <Typography
      sx={{
        wordBreak: 'break-word',
        marginBottom: 2,
      }}
      fontSize={14}
    >
      {message.content}
    </Typography>
  );

  const renderChatTimestamp = () => (
    <Typography
      fontSize={10}
      style={{
        textAlign: 'right',
        position: 'absolute',
        bottom: 2,
        right: 10,
      }}
    >
      {moment(message.timestamp).calendar()}
    </Typography>
  );

  if (message.from !== user?.userId) {
    return (
      <div style={{ display: 'flex', justifyContent: 'flex-end' }}>
        <StyledChatBubbleRight>
          {/* <Typography fontSize={14}>{message.from}</Typography> */}
          {renderChatContent()}
          <div>
            {renderChatTimestamp()}
            {/* TODO: wire up the delivered/read statuses */}
            <StyledCheckMark read={message.read}></StyledCheckMark>
          </div>
        </StyledChatBubbleRight>
      </div>
    );
  }

  return (
    <div style={{ display: 'flex', justifyContent: 'flex-start' }}>
      <StyledChatBubbleLeft>
        {renderChatContent()}
        <div>{renderChatTimestamp()}</div>
      </StyledChatBubbleLeft>
    </div>
  );
}
