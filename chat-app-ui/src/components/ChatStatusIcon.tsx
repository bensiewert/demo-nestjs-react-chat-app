import { styled } from '@mui/material';
import React from 'react';
import { useAuth } from 'src/contexts/AuthContext';

const StyledChatStatusIcon = styled('i')<Props>(({ connected }) => ({
  height: '8px',
  width: '8px',
  borderRadius: '50%',
  display: 'inline-block',
  backgroundColor: connected ? '#86bb71' : '#e38968',
  marginRight: '6px',
}));

interface Props {
  connected: boolean;
}

export default function ChatStatusIcon(props: Props) {
  const { connected } = props;
  const { user } = useAuth();

  return <StyledChatStatusIcon connected={connected} />;
}
