import React from 'react';
import {
  Dialog,
  IconButton,
  List,
  ListItem,
  ListItemButton,
  Toolbar,
  Typography,
} from '@mui/material';
import InputBase from '@mui/material/InputBase';
import { styled, alpha } from '@mui/material/styles';
import NewChatIcon from '@mui/icons-material/AddCommentOutlined';
import MoreIcon from '@mui/icons-material/MoreHoriz';
import SearchIcon from '@mui/icons-material/Search';
import NewChatForm from './NewChatForm';
import { useAuth } from 'src/contexts/AuthContext';
import { useChat } from 'src/contexts/ChatContext';
import { ChatUser } from 'src/interfaces/ChatUser';
import ChatStatusIcon from './ChatStatusIcon';

const Search = styled('div')(({ theme }) => ({
  position: 'relative',
  borderRadius: theme.shape.borderRadius,
  backgroundColor: alpha(theme.palette.common.white, 0.15),
  '&:hover': {
    backgroundColor: alpha(theme.palette.common.white, 0.25),
  },
  marginLeft: 0,
  width: '100%',
  [theme.breakpoints.up('sm')]: {
    marginLeft: theme.spacing(1),
    width: 'auto',
  },
}));

const SearchIconWrapper = styled('div')(({ theme }) => ({
  padding: theme.spacing(0, 2),
  height: '100%',
  position: 'absolute',
  pointerEvents: 'none',
  display: 'flex',
  alignItems: 'center',
  justifyContent: 'center',
}));

const StyledInputBase = styled(InputBase)(({ theme }) => ({
  color: 'inherit',
  '& .MuiInputBase-input': {
    padding: theme.spacing(1, 1, 1, 0),
    // vertical padding + font size from searchIcon
    paddingLeft: `calc(1em + ${theme.spacing(4)})`,
    transition: theme.transitions.create('width'),
    width: '100%',
  },
}));

interface Props {
  users?: ChatUser[];
}

export default function SideChatBar(props: Props) {
  const { users } = props;
  const { user } = useAuth();
  const { chatUser, setChatUser } = useChat();
  const [newChatOpen, setNewChatOpen] = React.useState(false);

  const handleNewChatClick = () => {
    setNewChatOpen(true);
  };

  const handleCloseNewChat = () => {
    setNewChatOpen(false);
  };

  const setChatContext = (user: ChatUser) => {
    setChatUser(user);
  };
  return (
    <>
      <div style={{ minWidth: 300, height: '100vh', overflowY: 'scroll' }}>
        <Toolbar style={{ padding: '0 10px' }}>
          <Typography>Chats</Typography>
          <div style={{ flexGrow: 1 }}></div>
          <IconButton
            color="primary"
            sx={{ p: '10px' }}
            aria-label="directions"
            onClick={handleNewChatClick}
          >
            <NewChatIcon />
          </IconButton>
          <IconButton
            color="primary"
            sx={{ p: '10px' }}
            aria-label="directions"
          >
            <MoreIcon />
          </IconButton>
        </Toolbar>
        <Toolbar style={{ padding: '0 10px' }}>
          <Search>
            <SearchIconWrapper>
              <SearchIcon />
            </SearchIconWrapper>
            <StyledInputBase
              placeholder="Search..."
              inputProps={{ 'aria-label': 'search' }}
            />
          </Search>
        </Toolbar>
        <div>
          <List>
            {users &&
              users.map((u: ChatUser) => (
                <ListItemButton
                  onClick={() => setChatContext(u)}
                  key={u.userId}
                  selected={chatUser?.userId === u.userId}
                >
                  <ListItem>
                    {u.username}
                    {user?.userId === u.userId ? ` (You)` : null}
                  </ListItem>
                  <ChatStatusIcon connected={u.connected} />
                </ListItemButton>
              ))}
          </List>
        </div>
      </div>
      <Dialog open={newChatOpen} onClose={handleCloseNewChat} fullWidth>
        <NewChatForm
          setOpen={setNewChatOpen}
          handleClose={handleCloseNewChat}
        />
      </Dialog>
    </>
  );
}
