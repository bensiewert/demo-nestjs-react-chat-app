import React, {
  ReactNode,
  useContext,
  useMemo,
  createContext,
  useState,
} from 'react';
import { IChatContext } from 'src/interfaces/ChatContext';
import { ChatMessage } from 'src/interfaces/ChatMessage';
import { ChatUser } from 'src/interfaces/ChatUser';

const ChatContext = createContext({} as IChatContext);

export function ChatProvider({ children }: { children: ReactNode }) {
  const [chatUser, setChatUser] = useState<ChatUser>();
  const [chatUsers, setChatUsers] = useState<ChatUser[]>([]);
  const [messages, setMessages] = useState<ChatMessage[]>([]);

  const memoedValue = useMemo(
    () => ({
      chatUser,
      setChatUser,
      messages,
      setMessages,
      chatUsers,
      setChatUsers,
    }),
    [chatUser, messages, chatUsers],
  );

  return (
    <ChatContext.Provider value={memoedValue}>{children}</ChatContext.Provider>
  );
}

export function useChat() {
  return useContext(ChatContext);
}

export { ChatContext };
