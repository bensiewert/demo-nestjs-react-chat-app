import React, {
  ReactNode,
  useContext,
  useMemo,
  createContext,
  useState,
  useEffect,
} from 'react';
import io from 'socket.io-client';
import { User } from 'src/api/models/user';
import { LocalStorageKeys } from 'src/config';
import { ChatMessage } from 'src/interfaces/ChatMessage';
import { ChatUser } from 'src/interfaces/ChatUser';
import { IAppSocket, ISocketIOContext } from 'src/interfaces/SocketIO';
import { getAccessToken } from 'src/utils/local-storage';
import { useAuth } from './AuthContext';
import { useChat } from './ChatContext';

const SocketIOContext = createContext({} as ISocketIOContext);

const socket: IAppSocket = io('http://localhost:5000', { autoConnect: false });

export function SocketIOProvider({ children }: { children: ReactNode }) {
  // eslint-disable-next-line @typescript-eslint/no-unused-vars
  const [isConnected, setIsConnected] = useState(socket.connected);
  const [newChatUser, setNewChatUser] = useState<ChatUser>();
  const [userDisconnected, setUserDisconnected] = useState<string>();
  const [newChatMessage, setNewChatMessage] = useState<ChatMessage>();
  const { user } = useAuth();
  const { chatUsers, setChatUsers, chatUser, setChatUser } = useChat();

  React.useEffect(() => {
    const sessionId = localStorage.getItem(LocalStorageKeys.WS_SESSSION_ID);

    socket.auth = { sessionId, token: getAccessToken() };
    socket.connect();

    socket.on('connect', () => {
      setIsConnected(true);
    });

    socket.on('disconnect', () => {
      setIsConnected(false);
      socket.removeAllListeners();
    });

    socket.on('session', ({ sessionId, userId }) => {
      // attach the session ID to the next reconnection attempts
      socket.auth = { sessionId, token: getAccessToken() };
      // store it in the localStorage
      localStorage.setItem(LocalStorageKeys.WS_SESSSION_ID, sessionId);
      // save the ID of the user

      // TODO: types for socket data
      // eslint-disable-next-line @typescript-eslint/ban-ts-comment
      // @ts-ignore
      socket.userId = userId;
    });

    socket.on('users', (users: ChatUser[]) => {
      users.sort((a, b) => {
        if (a.userId === user?.userId) return -1;
        if (a.username < b.username) return -1;
        return a.username > b.username ? 1 : 0;
      });

      setChatUsers(users);
      if (users && users.length === 1) {
        setChatUser(users[0]);
      }
      if (users && users.length > 1) {
        setChatUser(users[1]);
      }
    });

    socket.on('user connected', (user: ChatUser) => {
      setNewChatUser(user);
    });

    socket.on('user disconnected', (userId: string) => {
      setUserDisconnected(userId);
    });

    socket.on('private message', (message: ChatMessage) => {
      setNewChatMessage(message);
    });

    return () => {
      socket.off('connect');
      socket.off('disconnect');
      socket.off('users');
      socket.off('user connected');
      socket.off('user disconnected');
      socket.off('private message');
      socket.removeAllListeners();
    };
  }, []);

  useEffect(() => {
    onUserConnected(newChatUser);
  }, [newChatUser]);

  useEffect(() => {
    if (!userDisconnected) {
      return;
    }
    const newChatUsers = [...chatUsers];
    const user = newChatUsers.find((u) => u.userId === userDisconnected);

    if (user) {
      user.connected = false;
      setChatUsers(newChatUsers);
    }
  }, [userDisconnected]);

  useEffect(() => {
    if (newChatMessage) {
      const newChatUsers = [...chatUsers];

      const chatUserToUpdate = newChatUsers.find(
        ({ userId }) => newChatMessage.from === userId,
      );

      if (!chatUserToUpdate) {
        return;
      }

      const newChatUser = { ...chatUserToUpdate };
      newChatUser.messages.push(newChatMessage);
      setChatUsers(newChatUsers);
    }
  }, [newChatMessage]);

  const onUserConnected = (user: ChatUser | undefined) => {
    if (!user) return;

    const newChatUsers = [...chatUsers];
    const userInChatUsers = newChatUsers.find((u) => u.userId === user.userId);

    if (!userInChatUsers) {
      setChatUsers((u) => [...u, user]);
    } else {
      userInChatUsers.connected = true;
      setChatUsers(newChatUsers);
    }
  };

  const disconnect = () => {
    socket.disconnect();
  };

  const emitChatMessage = (recipient: User, content: string) => {
    const message = {
      content,
      from: user?.userId || '',
      to: recipient.userId,
      timestamp: Date.now(),
      read: false,
    };

    socket.emit('private message', message);
    if (chatUser) {
      const newChatUser = { ...chatUser };
      newChatUser.messages?.push(message);
      setChatUser(newChatUser);
    }
  };

  const memoedValue = useMemo(
    () => ({
      disconnect,
      emitChatMessage,
    }),
    [disconnect],
  );

  return (
    <SocketIOContext.Provider value={memoedValue}>
      {children}
    </SocketIOContext.Provider>
  );
}

export function useSocketIO() {
  return useContext(SocketIOContext);
}

export { SocketIOContext };
