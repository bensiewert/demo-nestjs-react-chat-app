import React, { ReactNode, useContext, useMemo, createContext } from 'react';
import { useRouter } from 'next/router';
import { AxiosError } from 'axios';

import Container from '@mui/material/Container';
import CircularProgress from '@mui/material/CircularProgress';

import {
  register as sendRegister,
  login as sendLogin,
  logout as sendLogout,
  getUser,
  resetPassword as sendResetPassword,
  confirmPasswordReset as sendConfirmPasswordReset,
  resetPasswordUpdatePassword,
  confirmEmail,
} from 'src/api/auth';
import { LocalStorageKeys } from 'src/config';
import jwtDecode, { JwtPayload } from 'jwt-decode';
import { AppErrorResponse } from 'src/http/client';
import { User } from 'src/api/models/user';
import {
  IAuthContext,
  ICredentials,
  IRegisterParams,
} from 'src/interfaces/Auth';
import PrivateLayout from 'src/layouts/PrivateLayout';
import PublicLayout from 'src/layouts/PublicLayout';

const AuthContext = createContext({} as IAuthContext);

export function AuthProvider({ children }: { children: ReactNode }) {
  const [loading, setLoading] = React.useState(false);
  const [isCheckingSession, setIsCheckingSession] = React.useState(true);
  const [formStep, setFormStep] = React.useState(0);
  const [user, setUser] = React.useState<User | null>(null);
  const [errorMessage, setErrorMessage] = React.useState<string | null>(null);

  const router = useRouter();

  React.useEffect(() => {
    const checkSession = async () => {
      try {
        const appData = localStorage.getItem(LocalStorageKeys.APP_DATA);
        if (appData) {
          const { auth }: { auth: { access_token: string; refresh: string } } =
            JSON.parse(appData || '');
          const { exp } = jwtDecode<JwtPayload>(auth.access_token);
          if (exp && exp * 1000 < Date.now()) {
            localStorage.removeItem(LocalStorageKeys.APP_DATA);
          }
          const userInfo = await getUser();
          setUser(userInfo.data);
        } else {
          setUser(null);
        }
      } catch (error) {
        return setUser(null);
      } finally {
        setIsCheckingSession(false);
      }
    };

    checkSession();
  }, []);

  const login = async (params: ICredentials) => {
    setLoading(true);
    try {
      const r = await sendLogin(params);
      localStorage.setItem(
        LocalStorageKeys.APP_DATA,
        JSON.stringify({ auth: r.data }),
      );
      const userInfo = await getUser();
      setUser(userInfo.data);
      router.replace('/home');
      setLoading(false);
    } catch (e) {
      setErrorMessage('Invalid username or password');
      setLoading(false);
    }
  };

  const resetPassword = async (email: string) => {
    setLoading(true);
    try {
      await sendResetPassword(email);
      setFormStep(1);
      setLoading(false);
    } catch (error) {
      setErrorMessage('error while resetting password');
      setLoading(false);
    }
  };

  const confirmPasswordReset = async (token: string) => {
    setLoading(true);
    try {
      await sendConfirmPasswordReset(token);
    } catch (error) {
      const e = error as AxiosError<AppErrorResponse>;
      setErrorMessage(e.response?.data.message || '');
    } finally {
      setLoading(false);
    }
  };

  const resetPasswordUpdate = async (values: {
    password: string;
    confirm_password: string;
    token: string;
  }) => {
    setLoading(true);
    try {
      await resetPasswordUpdatePassword(values);
      setFormStep(1);
    } catch (error) {
      console.log('error: ', error);
      const e = error as AxiosError<AppErrorResponse>;
      setErrorMessage(e.response?.data.message || '');
    } finally {
      setLoading(false);
    }
  };

  const register = async (values: IRegisterParams) => {
    setLoading(true);
    try {
      await sendRegister(values);
      setFormStep(1);
      setLoading(false);
    } catch (e) {
      const error = e as AxiosError;
      if (error.isAxiosError && error.response?.status === 409) {
        setErrorMessage('An account with that email address exists.');
      }
      setFormStep(0);
      setLoading(false);
    }
  };

  const confirmAccount = async (token: string) => {
    try {
      await confirmEmail(token);
      return {
        success: true,
        reason: '',
      };
    } catch (e) {
      const error = e as AxiosError;
      if (error.isAxiosError && error.response?.status === 404) {
        return {
          success: false,
          reason: 'Token not found.',
        };
      }
      return {
        success: false,
        reason: 'Error, please try again later.',
      };
    }
  };

  const logout = async () => {
    try {
      await sendLogout();
      setUser(null);
      router.replace('/auth/login');
    } catch (error) {
      setUser(null);
      router.replace('/auth/login');
    }
  };

  const memoedValue = useMemo(
    () => ({
      user,
      loading,
      errorMessage,
      setErrorMessage,
      isCheckingSession,
      register,
      confirmAccount,
      login,
      logout,
      resetPassword,
      confirmPasswordReset,
      resetPasswordUpdate,
      formStep,
      setFormStep,
    }),
    [user, loading, errorMessage, formStep],
  );

  function renderLoader() {
    return (
      <div style={{ height: '100vh' }}>
        <Container
          maxWidth="sm"
          style={{
            display: 'flex',
            flexDirection: 'column',
            height: '100%',
            justifyContent: 'center',
            alignItems: 'center',
          }}
        >
          <CircularProgress />
        </Container>
      </div>
    );
  }

  function renderContent() {
    if (isCheckingSession) {
      return renderLoader();
    }

    if (
      user &&
      (router.pathname === '/auth/login' ||
        router.pathname === '/auth/confirm' ||
        router.pathname === '/auth/register')
    ) {
      router.push('/home');
      return renderLoader();
    }

    if (!user && router.pathname === '/home') {
      router.push('/auth/login');
      return renderLoader();
    }

    if (user) {
      return <PrivateLayout>{children}</PrivateLayout>;
    }

    return <PublicLayout>{children}</PublicLayout>;
  }

  return (
    <AuthContext.Provider value={memoedValue}>
      <div
        style={{ height: '100vh', display: 'flex', flexDirection: 'column' }}
      >
        {renderContent()}
      </div>
    </AuthContext.Provider>
  );
}

export function useAuth() {
  return useContext(AuthContext);
}

export { AuthContext };
