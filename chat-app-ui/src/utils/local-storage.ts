import { LocalStorageKeys } from 'src/config';

export function getAccessToken(): string | null {
  const { auth } = JSON.parse(
    localStorage.getItem(LocalStorageKeys.APP_DATA) || '{}',
  );
  return auth ? auth.access_token : null;
}
