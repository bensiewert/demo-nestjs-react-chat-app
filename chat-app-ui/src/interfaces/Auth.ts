import { User } from 'src/api/models/user';

export interface IAuthContext {
  loading: boolean;
  user: User | null;
  errorMessage: string | null;
  setErrorMessage: React.Dispatch<React.SetStateAction<string | null>>;
  isCheckingSession: boolean;
  login: (params: ICredentials) => { message: string } | Promise<void>;
  resetPassword: (email: string) => { message: string } | Promise<void>;
  register: (values: IRegisterParams) => { message: string } | Promise<void>;
  formStep: number;
  setFormStep: React.Dispatch<React.SetStateAction<number>>;
  logout: () => void;
  confirmAccount: (
    token: string,
  ) => Promise<{ success: boolean; reason: string }>;
  confirmPasswordReset: (token: string) => Promise<void>;
  resetPasswordUpdate: (values: {
    password: string;
    confirm_password: string;
    token: string;
  }) => Promise<void>;
}

export interface ICredentials {
  email: string;
  password: string;
}

export interface IRegisterParams extends ICredentials {
  username: string;
  confirmPassword: string;
}
