import { ChatMessage } from './ChatMessage';
import { ChatUser } from './ChatUser';

export interface IChatContext {
  chatUser: ChatUser | undefined;
  setChatUser: React.Dispatch<React.SetStateAction<ChatUser | undefined>>;
  messages: ChatMessage[];
  setMessages: React.Dispatch<React.SetStateAction<ChatMessage[]>>;
  chatUsers: ChatUser[];
  setChatUsers: React.Dispatch<React.SetStateAction<ChatUser[]>>;
}
