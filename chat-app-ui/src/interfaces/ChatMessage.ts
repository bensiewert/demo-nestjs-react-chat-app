export interface ChatMessage {
  content: string;
  from: string;
  to: string;
  timestamp: number;
  read: boolean;
}
