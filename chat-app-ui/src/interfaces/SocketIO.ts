import { Socket } from 'socket.io-client';
import { User } from 'src/api/models/user';

export interface ISocketIOContext {
  disconnect: () => void;
  emitChatMessage: (user: User, message: string) => void;
}

export interface IAppSocket extends Socket {
  userId?: string;
}
