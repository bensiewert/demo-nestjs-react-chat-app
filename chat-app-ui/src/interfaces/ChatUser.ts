import { User } from 'src/api/models/user';
import { ChatMessage } from './ChatMessage';

export interface ChatUser extends User {
  connected: boolean;
  self: boolean;
  messages: ChatMessage[];
  sessionId: string;
  userId: string;
  username: string;
}
