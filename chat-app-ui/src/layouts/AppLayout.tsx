import React, { ReactNode } from 'react';
import { useAuth } from 'src/contexts/AuthContext';
import { CircularProgress, Container } from '@mui/material';
import PrivateLayout from './PrivateLayout';
import PublicLayout from './PublicLayout';

export default function AppLayout({ children }: { children: ReactNode }) {
  const { user, isCheckingSession } = useAuth();

  if (isCheckingSession && !user) {
    return (
      <div style={{ height: '100vh' }}>
        {/* <Head>
          <title>Loading | {title}</title>
        </Head> */}
        <Container
          maxWidth="sm"
          style={{
            display: 'flex',
            flexDirection: 'column',
            height: '100%',
            justifyContent: 'center',
            alignItems: 'center',
          }}
        >
          <CircularProgress />
        </Container>
      </div>
    );
  }

  if (user) {
    return <PrivateLayout>{children}</PrivateLayout>;
  }

  return <PublicLayout>{children}</PublicLayout>;
}
