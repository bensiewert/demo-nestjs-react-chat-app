import React from 'react';

import AppBar from '@mui/material/AppBar';
import Toolbar from '@mui/material/Toolbar';
import Typography from '@mui/material/Typography';
import Button from '@mui/material/Button';
import Container from '@mui/material/Container';
import Link from '../components/Link';
import { styled } from '@mui/material';
import { useAuth } from 'src/contexts/AuthContext';
import { useRouter } from 'next/router';

const StyledAppBar = styled(AppBar)(() => ({
  backgroundColor: 'transparent',
}));

export default function PublicLayout({
  children,
}: Partial<React.PropsWithChildren<JSX.Element>>) {
  const { user } = useAuth();
  const router = useRouter();

  if (user) {
    router.replace('/home');
    return null;
  }

  return (
    <>
      <StyledAppBar position="static" elevation={0}>
        <Toolbar>
          <Typography
            variant="h6"
            style={{
              flexGrow: 1,
              color: '#000',
            }}
          >
            <Link href="/">Mingo</Link>
          </Typography>
          <Button color="primary" component={Link} href="/auth/register">
            Register
          </Button>
          <Button color="primary" component={Link} href="/auth/login">
            Login
          </Button>
        </Toolbar>
      </StyledAppBar>
      <div style={{ display: 'flex', flex: 1, marginTop: -64 }}>
        <Container
          maxWidth="sm"
          style={{
            display: 'flex',
            flexDirection: 'column',
            justifyContent: 'center',
          }}
        >
          <>{children}</>
        </Container>
      </div>
    </>
  );
}
