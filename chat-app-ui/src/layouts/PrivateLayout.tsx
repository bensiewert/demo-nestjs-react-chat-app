import React from 'react';

import { useAuth } from 'src/contexts/AuthContext';
import { useRouter } from 'next/router';
import { SocketIOProvider } from 'src/contexts/SocketIoContext';
import { ChatProvider } from 'src/contexts/ChatContext';

export default function PrivateLayout({
  children,
}: Partial<React.PropsWithChildren<JSX.Element>>) {
  const { user } = useAuth();
  const router = useRouter();

  if (!user) {
    router.replace('/auth/login');
    return null;
  }

  return (
    <ChatProvider>
      <SocketIOProvider>{children}</SocketIOProvider>
    </ChatProvider>
  );
}
