export const appMeta = {
  title: 'React Boilerplate',
  description: 'An amazing react starter',
  landing: {
    title: 'Get started with React',
  },
  login: {
    title: 'Login',
  },
};

export const API_URL = process.env.NEXT_PUBLIC_API_URL;

export enum LocalStorageKeys {
  APP_DATA = 'APP_DATA',
  WS_SESSSION_ID = 'sessionId',
}

export enum PUBLIC_ROUTE_NAMES {
  LOGIN = '/auth/login',
  CONFIRM = '/auth/confirm',
  REGISTER = '/auth/register',
}

export enum PRIVATE_ROUTE_NAMES {
  HOME = '/home',
}

export const APP_ROUTES = {
  public: {
    login: PUBLIC_ROUTE_NAMES.LOGIN,
    confirm: PUBLIC_ROUTE_NAMES.CONFIRM,
    register: PUBLIC_ROUTE_NAMES.REGISTER,
  },
  private: {
    home: PRIVATE_ROUTE_NAMES.HOME,
  },
};
