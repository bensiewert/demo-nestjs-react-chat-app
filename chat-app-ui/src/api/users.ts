import { request } from 'src/http/client';
import { User } from 'src/api/models/user';

/**
 * Get userse
 * @returns Promise<void>
 */
export const getUsers = () => {
  return request<User[]>('/api/users', {
    method: 'GET',
  });
};
