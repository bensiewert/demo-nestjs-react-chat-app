import { request } from 'src/http/client';
import { User } from 'src/api/models/user';
import { ICredentials, IRegisterParams } from 'src/interfaces/Auth';
import { LocalStorageKeys } from 'src/config';

/**
 * Register a new user
 * @param email
 * @param password
 * @returns Promise<void>
 */
export const register = (params: IRegisterParams) => {
  return request('/api/auth/signup', {
    data: params,
    method: 'POST',
  });
};

/**
 * confirm a new user
 * @param token
 * @returns Promise<void>
 */
export const confirmEmail = (token: string) => {
  return request<{ message: string }>(
    `/api/auth/signup/confirm?token=${token}`,
    {
      method: 'GET',
    },
  );
};

/**
 * Login a user
 * @param email
 * @param password
 * @returns Promise
 */
export const login = (params: ICredentials) => {
  return request<{ refresh: string; access_token: string }>('/api/auth/login', {
    data: params,
    method: 'POST',
  });
};

/**
 * Reset password request
 * @param email
 * @returns Promise
 */
export const resetPassword = (email: string) => {
  return request<{ success: boolean; message: string }>(
    '/api/auth/account/reset-pw',
    {
      data: {
        email,
      },
      method: 'POST',
    },
  );
};

/**
 * confirm pw reset
 * @param token
 * @returns Promise<void>
 */
export const confirmPasswordReset = (token: string) => {
  return request(`/api/auth/account/reset-pw-confirm?token=${token}`, {
    method: 'GET',
  });
};

/**
 * update password
 * @param email
 * @returns Promise
 */
export const resetPasswordUpdatePassword = (values: {
  password: string;
  confirm_password: string;
  token: string;
}) => {
  return request<{ success: boolean; message: string }>(
    '/api/auth/account/reset-pw-update',
    {
      data: {
        ...values,
      },
      method: 'PUT',
    },
  );
};

/**
 * Logout a user
 * @returns void
 */
export const logout = () => {
  localStorage.removeItem(LocalStorageKeys.APP_DATA);
};

/**
 * Get user info
 * @returns object
 */
export const getUser = () => {
  return request<User>('/api/users/me', {
    method: 'GET',
  });
};
